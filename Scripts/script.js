/**
 * @author Aidan Catriel
 * @desc This script uses a NASA api to retrieve pictures and their descriptions. Up to five pictures can 
 * 
 * API Key: iESKg3JuJ9QlGRJe0LxGc1Z10EgAdJqqTu389IkL
 * Example search: https://api.nasa.gov/planetary/apod?api_key=iESKg3JuJ9QlGRJe0LxGc1Z10EgAdJqqTu389IkL&count=1
 * 
 * Globals is a global array that can be accessed and modified anywhere
 */

"use strict";
let globals;

document.addEventListener("DOMContentLoaded", setup);

/**
 * @author Aidan Catriel
 * @desc Sets up the script. Turns globals into an array and populates it with various attributes. Checks if
 * anything is in local storage, then updates the DOM to match.
 */
function setup() {

    globals = {};
    globals.search = new URL("https://api.nasa.gov/planetary/apod");
    globals.search.searchParams.set("api_key","iESKg3JuJ9QlGRJe0LxGc1Z10EgAdJqqTu389IkL");
    globals.search.searchParams.set("count",1);
    globals.imageCount = 0;
    globals.images = new Map()
    globals.body = document.querySelector("body");
    let retrievalButton = globals.body.querySelector("#retrieval");
    retrievalButton.addEventListener("click", loadAPI);

    globals.images = readFromLocalStorage("images");

    globals.images.forEach(temp => globals.imageCount ++);
    callUpdateDOM();
}

/**
 * @author Aidan Catriel
 * @desc Retrieves a promise from the API, converts it into a usable object then passes it to addObjectToDOM.
 */
function loadAPI() {
    let pictureData = fetch(globals.search);
    pictureData.then(response => 
        {
            if (!response.ok)
            {
                console.log("Error with picture retrieval");
                return undefined;
            }
            else
            {
                return response.json();
            }
        }).then(jsonObject => addObjectToMap(jsonObject));
}

/**
 * @author Aidan Catriel
 * @desc Takes an object and adds the image and description to a map. If there are more than five elements 
 * it replaces one. For each element in globals.images, calls updateDOM.
 * @param {object} inputObject - The object to comb through for information to be used.
 */
function addObjectToMap(inputObject) {
    
    globals.imageCount ++;

    let imageObj = {
        "title":inputObject[0].title,
        "description":inputObject[0].explanation,
        "hdurl":inputObject[0].hdurl,
        "media_type":inputObject[0].media_type,
        "copyright":inputObject[0].copyright
    };
    
    globals.images.set(globals.imageCount,imageObj);
    if (globals.imageCount >= 6)
    {
        updateMap();
    }
    
    callUpdateDOM();
    
    localStorage.setItem("images",JSON.stringify(Array.from(globals.images)))
}

/**
 * @author Aidan Catriel
 * @desc Calls updateDOM for each element in globals.images.
 */
function callUpdateDOM()
{
    globals.images.forEach((value, key) => {
        updateDOM(key);
    });
}
/**
 * @author Aidan Catriel
 * @desc Takes a number and adds the corresponding global.images object to the DOM. If the element does not
 * have a corresponding button, creates one. Each button will call updateDOM with its own position when clicked.
 * @param {int} position - The position to search for in globals.images.
 */
function updateDOM(position)
{
    let image = document.querySelector("#image");
    let copyright = document.querySelector("#copyright");
    let description = document.querySelector("#description");
    let button = document.querySelector("#btn"+position)

    if (button == null)
    {
        button = document.createElement("button");
        button.id = "btn"+position;

        let buttonList = document.querySelector("#buttonList");
        buttonList.appendChild(button);
        button.addEventListener("click", submit)
    }

    let imageObj = globals.images.get(position);
    button.textContent = imageObj.title;
    
    if (imageObj.media_type == "image")
    {
        image.src = imageObj.hdurl;
        if (imageObj.copyright == null)
        {
            copyright.textContent = "Public domain"
        }
        else
        {
            copyright.textContent = imageObj.copyright;
        }
    }
    else
    {
        image.src = "Images/noImage.png";
        copyright.textContent = "Wikipedia.com";
    }
    
    description.textContent = imageObj.description;
}

/**
 * @author Aidan Catriel
 * @desc Reorders global.images so that each element contains the next one's value, then deletes the sixth
 * element and updates globals.imageCount.
 */
function submit(event)
{
    let positionArr = event.target.id.match(/\d+/g);
    let position = parseInt(positionArr[0]);
    updateDOM(position);
}

/**
 * @author Aidan Catriel
 * @desc Reorders global.images so that each element contains the next one's value, then deletes the sixth
 * element and updates globals.imageCount.
 */
function updateMap()
{
    globals.images.forEach((value, key, map) => {
        let nextKey = key+1;
        globals.images.set(key,map.get(nextKey));

        if (key >= 6)
        {
            map.delete(key);
        }
    });
    globals.imageCount --;
}

/**
 * @author Aidan Catriel
 * @desc Searches for a map in local storage and returns it
 * @param {string} item - The item to search for in local storage.
 * @returns A map to be used
 */
function readFromLocalStorage(item) {
    let tempMap = new Map();
    if (localStorage.getItem(item)) {
        tempMap = new Map(JSON.parse(localStorage.getItem(item)))
    }
    return tempMap
}